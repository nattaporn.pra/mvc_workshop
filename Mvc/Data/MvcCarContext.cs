﻿using Microsoft.EntityFrameworkCore;
using Mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mvc.Data
{
    public class MvcCarContext : DbContext
    {
        public MvcCarContext(DbContextOptions<MvcCarContext> options)
            : base(options)
        {
        }

        public DbSet<Users> User { get; set; }
        public DbSet<Cars> Car { get; set; }
    }
}
